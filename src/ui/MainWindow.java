/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/11/13
 * Time: 9:04 PM
 */
package ui;

import datamodel.Currency;
import dataproviders.*;

import java.awt.*;
import java.awt.event.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.*;
import javax.swing.*;
import java.util.*;
import java.util.List;

public class MainWindow extends JFrame {

	private JTextField txtAmount;
	private JTextField txtDate;
	private JComboBox cmbConvertFrom;
	private JComboBox cmbConvertTo;
	private JLabel lblResult;
	private JLabel lblMessages;
	private JLabel lblDate;
	private JButton btnDoCalculate;

	private BigDecimal result;
	private Date user_date = null;

	private int PADDING_LEFT = 20;
	private int PADDING_TOP = 10;
	private int PADDING_RIGHT = 10;
	private int PADDING_BOTTOM = 10;
	private int CONTROL_HEIGHT = 25;
	private int CONTROL_WIDTH = 100;

	List<Currency> currencies = null;

	public MainWindow(List<Currency> cur) {

		if (cur != null) this.currencies = cur;

		// Initialize GUI controls
		InitControls();

		// Event handlers
		btnDoCalculate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent evt) {

				// Check numeric data from amount textbox first
				String amount = txtAmount.getText();

				if (!amount.isEmpty() && InputValidationHelper.isNumber(amount)) {

					// Reset message label text properties
					SetMessage("", Color.BLACK);

					// Normalize number string by replacing ',' -> '.'
					String normAmount = amount.replace(',', '.');

					try {
						result = new BigDecimal(normAmount);
						lblMessages.setText(result.toString());
					} catch (NumberFormatException exc) {
						SetMessage("Incorrect numeric value was used.", Color.RED);
					}

					// Reset message label text properties
					SetMessage("", Color.BLACK);

					// Validate data format
					if (txtDate.getText().isEmpty()) {

						// Do calculation using current rates if date wasn't specified
						DoCalculation();
					} else {

						SimpleDateFormat sdf = new SimpleDateFormat("dd.mm.yyyy");

						// Perform strict validation
						sdf.setLenient(false);
						Date start_date = null;

						try {
							user_date = sdf.parse(txtDate.getText());
							start_date = sdf.parse("01.01.1999");
						} catch (ParseException exc) {
							exc.printStackTrace();
							SetMessage("Please enter date in format: dd.mm.yyyy", Color.RED);
						}

						Calendar today = Calendar.getInstance();
						today.set(Calendar.HOUR_OF_DAY, 0);

						// Check if specified date is before today and 1999 year because of limits on data provider side
						if (user_date.before(today.getTime()) && user_date.after(start_date)) {

							// Do calculation for specified date
							OERDataProvider dataProvider = (OERDataProvider) DataProviderFactory.BuildByType(DataProviderType.OER);
							String currentRate = dataProvider.GetRateAtDate(user_date);
							currencies = DataProviderManager.SerializeFromJson(currentRate);
							DoCalculation();

						} else {
							SetMessage("Specified date exceeds available history.", Color.RED);
						}
					}

				} else {
					SetMessage("Only positive numbers are allowed.", Color.RED);
				}
			}
		});
	}

	private void DoCalculation() {

		Currency selected = null;
		Currency target = null;

		String nameFrom = cmbConvertFrom.getSelectedItem().toString().substring(0, 3);
		String nameTo = cmbConvertTo.getSelectedItem().toString().substring(0, 3);

		for (Currency cur : currencies) {

			if (cur.getShortName().equals(nameFrom)) {
				selected = currencies.get(currencies.indexOf(cur));
				selected.setAmount(result);
			}

			if (cur.getShortName().equals(nameTo)) {
				int idx = currencies.indexOf(cur);
				target = currencies.get(idx);
			}
		}

		BigDecimal result = null;
		if (selected != null) result = selected.ConvertTo(target);

		if (result != null)
			SetMessage(result.setScale(4, RoundingMode.HALF_DOWN).toString(), Color.BLACK);
		else
			SetMessage("Unable to fetch data from data provider.", Color.RED);
	}

	private void SetMessage(String message, Color color) {
		lblMessages.setText(message);
		lblMessages.setForeground(color);
	}

	private void InitControls() {

		// General layout of controls on the main windows form
		Container mainContainer = getContentPane();
		mainContainer.setBounds(new Rectangle(200, 200));

		// More complicated case is required GUI builder and more complex layout scheme
		mainContainer.setLayout(null);

		// Numeric text box
		txtAmount = new JTextField();
		txtAmount.setToolTipText("Amount of currency.");
		txtAmount.setBounds(PADDING_LEFT, PADDING_TOP, CONTROL_WIDTH, CONTROL_HEIGHT);
		txtAmount.setEditable(true);
		mainContainer.add(txtAmount);

		DefaultComboBoxModel defaultComboBoxModel = new DefaultComboBoxModel(StaticDefines.BuildActualCurrenciesList(currencies).toArray());

		cmbConvertFrom = new JComboBox(new ComboBoxModelDecorator(defaultComboBoxModel));
		cmbConvertFrom.setBounds(110 + PADDING_LEFT, PADDING_TOP, 250, CONTROL_HEIGHT);
		mainContainer.add(cmbConvertFrom);

		// Main button control
		btnDoCalculate = new JButton("Convert to");
		btnDoCalculate.setBounds(PADDING_LEFT, 50 + PADDING_TOP, CONTROL_WIDTH, CONTROL_HEIGHT);
		mainContainer.add(btnDoCalculate);

		// Combobox for target currency
		cmbConvertTo = new JComboBox(new ComboBoxModelDecorator(defaultComboBoxModel));
		cmbConvertTo.setBounds(110 + PADDING_LEFT, 50 + PADDING_TOP, 250, CONTROL_HEIGHT);
		mainContainer.add(cmbConvertTo);

		lblDate = new JLabel("At date:");
		lblDate.setBounds(48 + PADDING_LEFT, 100 + PADDING_TOP, 70, CONTROL_HEIGHT);
		mainContainer.add(lblDate);

		// Date specifier
		txtDate = new JTextField("", 12);
		txtDate.setEditable(true);
		txtDate.setToolTipText("Optional: conversion rate date in format dd.mm.yyyy");
		txtDate.setBounds(110 + PADDING_LEFT, 95 + PADDING_TOP, CONTROL_WIDTH, CONTROL_HEIGHT);
		mainContainer.add(txtDate);


		lblResult = new JLabel("Result:");
		lblResult.setBounds(48 + PADDING_LEFT, 140 + PADDING_TOP, CONTROL_WIDTH, CONTROL_HEIGHT);
		mainContainer.add(lblResult);

		lblMessages = new JLabel();
		lblMessages.setBounds(110 + PADDING_LEFT, 140 + PADDING_TOP, 400, CONTROL_HEIGHT);
		mainContainer.add(lblMessages);

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle("Currency converter");
		setSize(PADDING_LEFT + 380 + PADDING_RIGHT, PADDING_TOP + 200 + PADDING_BOTTOM);
		setVisible(true);
	}
}
