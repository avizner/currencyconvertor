package ui;

import java.util.regex.*;

/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/12/13
 * Time: 2:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class InputValidationHelper {

	public static boolean isNumber(String numericValue) {

		boolean retValue = false;

		if (!numericValue.isEmpty()) {
			String pattern = "^[0-9]+[\\.|,]?[0-9]+$";//"\\d+(\\.0*)?";
			Pattern p = Pattern.compile(pattern);
			Matcher matcher = p.matcher(numericValue);
			retValue = matcher.find();
		}

		return  retValue;
	}
}
