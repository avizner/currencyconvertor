/**
 * User: alex
 * Date: 11/15/13
 * Time: 12:11 AM
 */

package ui;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public class ComboBoxModelDecorator extends DefaultComboBoxModel implements ComboBoxModel, ListDataListener {

	private DefaultComboBoxModel defaultComboBoxModel;

	public ComboBoxModelDecorator(DefaultComboBoxModel defCmbModel) {
		super();
		this.defaultComboBoxModel = defCmbModel;
	}

	@Override
	public int getSize() {
		return this.defaultComboBoxModel.getSize();
	}

	@Override
	public Object getElementAt(int index) {
		return this.defaultComboBoxModel.getElementAt(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		if (getListDataListeners().length == 0) {
			defaultComboBoxModel.addListDataListener(this);
		}
		super.addListDataListener(l);
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		super.removeListDataListener(l);
		if (getListDataListeners().length == 0) {
			defaultComboBoxModel.removeListDataListener(this);
		}
	}

	@Override
	public void addElement(Object anObject) {
		defaultComboBoxModel.addElement(anObject);
	}

	@Override
	public void removeElement(Object anObject) {
		defaultComboBoxModel.removeElement(anObject);
	}

	@Override
	public int getIndexOf(Object anObject) {
		return defaultComboBoxModel.getIndexOf(anObject);
	}

	@Override
	public void insertElementAt(Object anObject, int index) {
		defaultComboBoxModel.insertElementAt(anObject, index);
	}

	@Override
	public void removeAllElements() {
		defaultComboBoxModel.removeAllElements();
	}

	@Override
	public void removeElementAt(int index) {
		defaultComboBoxModel.removeElementAt(index);
	}

	@Override
	public void intervalAdded(ListDataEvent e) {
		fireIntervalAdded(this, e.getIndex0(), e.getIndex1());
	}

	@Override
	public void intervalRemoved(ListDataEvent e) {
		fireIntervalRemoved(this, e.getIndex0(), e.getIndex1());
	}

	@Override
	public void contentsChanged(ListDataEvent e) {
		fireContentsChanged(this, e.getIndex0(), e.getIndex1());
	}
}
