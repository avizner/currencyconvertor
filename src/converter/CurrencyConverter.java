/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/11/13
 * Time: 8:53 PM
 * To change this template use File | Settings | File Templates.
 */

package converter;

import java.io.IOException;
import java.net.*;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import datamodel.Currency;
import ui.MainWindow;
import dataproviders.*;

import javax.swing.*;

public class CurrencyConverter {

	private static Logger logger = Logger.getLogger(LogEvent.class.getName());
	private static final BlockingQueue<List<Currency>> queue = new LinkedBlockingQueue<List<Currency>>(1024);

	public static void main(String ... args) throws InterruptedException, IOException {

		// Check network availability
		Socket socket = null;

		try {
			socket = new Socket("www.google.com", 80);
		} catch (UnknownHostException exc) {
			exc.printStackTrace();
		}

		if(socket != null && socket.isConnected()) {

			socket.close();

			// Request latest rates anonymously
			new Thread(new Runnable() {

				@Override
				public void run() {

					// Create data provider and obtain latest rates
					OERDataProvider dataProvider = (OERDataProvider) DataProviderFactory.BuildByType(DataProviderType.OER);
					String currentRate = dataProvider.GetLatestRate();

					try {
						queue.put(DataProviderManager.SerializeFromJson(currentRate));
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}).start();

		} else {
			JOptionPane.showMessageDialog(null, "Unable to fetch financial data, network is down. Application will be stopped.",
					"Network error", JOptionPane.ERROR_MESSAGE);
			System.exit(0);

		}

		// Run GUI safely
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				logger.info("MainWindow: Running main window in separated thread.");
				List<Currency> cur = null;

				try {
					cur = queue.take();
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				new MainWindow(cur);
			}
		});
	}
}
