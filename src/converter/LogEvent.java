/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/12/13
 * Time: 11:30 AM
 */

package converter;

import java.util.logging.Logger;

public class LogEvent {

	// In more complicated case the log4j framework is most preferable choice
	private final static Logger LOG_EVENT = Logger.getLogger(CurrencyConverter.class.getName());

}
