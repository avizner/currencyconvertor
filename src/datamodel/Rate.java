/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/12/13
 * Time: 10:14 AM
 * To change this template use File | Settings | File Templates.
 */
package datamodel;

import java.math.BigDecimal;
import java.util.Date;

public class Rate {
	private BigDecimal rate;
	private Date atDate;

	public Rate(BigDecimal rate, Date atDate) {
		this.rate = rate;
		this.atDate = atDate;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Date getAtDate() {
		return atDate;
	}

	public void setAtDate(Date atDate) {
		this.atDate = atDate;
	}
}
