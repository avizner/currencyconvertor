/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/12/13
 * Time: 10:13 AM
 * To change this template use File | Settings | File Templates.
 */
package datamodel;

import ui.StaticDefines;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Currency {

	private String ShortName;
	private String LongName;
	private Rate Rate;
	private BigDecimal Amount;

	public Currency(String shortName, String longName, Rate rate, BigDecimal amount) {
		Init(shortName, longName, rate, amount);
	}

	public Currency(String shortName, Rate rate, BigDecimal amount) {

		String longName = null;

		if (StaticDefines.CurrenciesMap.containsKey(shortName)) {
			longName = StaticDefines.CurrenciesMap.get(shortName);
		}

		Init(shortName, longName, rate, amount);
	}

	private void Init(String shortName, String longName, Rate rate, BigDecimal amount) {

		this.ShortName = shortName;
		this.LongName = longName;
		this.Rate = rate;
		this.Amount = amount;
	}

	public String getShortName() {
		return ShortName;
	}

	public void setShortName(String shortName) {
		ShortName = shortName;
	}

	public String getLongName() {
		return LongName;
	}

	public void setLongName(String longName) {
		LongName = longName;
	}

	public Rate getRate() {
		return Rate;
	}

	public void setRate(Rate rate) {
		Rate = rate;
	}

	public BigDecimal getAmount() {
		return Amount;
	}

	public void setAmount(BigDecimal amount) {
		Amount = amount;
	}

	public BigDecimal ConvertTo(Currency currency) {

		BigDecimal retValue = null;

		if (this.Amount != null || currency != null) {

			BigDecimal inUSD = this.Amount.divide(this.Rate.getRate(), 4, RoundingMode.HALF_DOWN);
			retValue = currency.Rate.getRate().multiply(inUSD);
		}

		return retValue;
	}
}
