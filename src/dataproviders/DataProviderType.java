package dataproviders;

/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/12/13
 * Time: 10:37 AM
 */
public enum DataProviderType {
	GOOGLE,     // Google exchange rate query
	YAHOO,      // Yahoo finance API
	ECB,        // European Central Bank Feed
	OER         // Open Source Exchange Rates API
}
