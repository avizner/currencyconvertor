/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/12/13
 * Time: 4:05 PM
 */
package dataproviders;

import converter.LogEvent;
import datamodel.Currency;
import datamodel.Rate;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Logger;

public class DataProviderManager {

	private static Logger logger = Logger.getLogger(LogEvent.class.getName());
	private static List<Currency> currencies = null;

	public List<Currency> getCurrencies() {
		return currencies;
	}

	public static List<Currency> SerializeFromJson(String json) {

		Date date = null;
		String base = null;
		List<Currency> retValues = new ArrayList<Currency>();

		if (json != null) {

			for (String str: json.split(",")) {

				// White list of important data
				if(str.split(":")[0].trim().contains("timestamp")) {

					String timestamp = str.split(":")[1].trim();
					long unix_time = Long.parseLong(timestamp);
					date = new Date(unix_time*1000);
				}

				if (str.split(":")[0].trim().contains("base")) {

					base = str.split(":")[1].replace("\"", "").trim();
				}

				if (str.split(":")[0].replace("\"", "").trim().matches("[A-Z]{3}")) {

					retValues.add(SerializeFromJsonElement(str, date));
				}
			}
		} else {
			throw new IllegalArgumentException();
		}

		logger.info("SerializeFromJson: " + retValues.size() + " currency objects were created.");
		return retValues;
	}

	private static Currency SerializeFromJsonElement(String json, Date timestamp) {

		BigDecimal bd = null;
		Currency retValue = null;

		if (!json.isEmpty()) {

			String[] ret = json.split(":");

			NumberFormat nf = NumberFormat.getInstance(Locale.US);

			try {
				bd = new BigDecimal(nf.parse(ret[1].trim()).toString());
			} catch (ParseException e) {
				e.printStackTrace();
			}

			//bd = new BigDecimal(ret[1].trim());
			String shortName = ret[0].replace("\"", "").trim();
			retValue = new Currency(shortName, new Rate(bd, timestamp), null);
		}

		return retValue;
	}
}
