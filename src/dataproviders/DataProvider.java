/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/12/13
 * Time: 10:33 AM
 */

package dataproviders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import converter.LogEvent;

public abstract class DataProvider {

	static Logger logger = Logger.getLogger(LogEvent.class.getName());

	abstract String GetLatestRate();
	abstract String GetRateAtDate(Date date);

	public String RequestJSON(String urlString, int timeout) {
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-length", "0");
			connection.setUseCaches(false);
			connection.setAllowUserInteraction(false);
			connection.setConnectTimeout(timeout);
			connection.setReadTimeout(timeout);
			connection.connect();
			int status = connection.getResponseCode();

			switch (status) {
				case 200:
				case 201:
					BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
					StringBuilder sb = new StringBuilder();
					String line;
					while ((line = br.readLine()) != null) {
						sb.append(line+"\n");
					}
					br.close();
					logger.info("Response received successfully.");
					return sb.toString();
				case 400:
					logger.warning("Client requested rates for an unsupported base currency.");
					break;
				case 404:
					logger.warning("Client requested a non-existent resource.");
					break;
				case 403:
					logger.warning("Access restricted.");
					break;
				case 429:
					logger.warning("Access restricted for repeated over-use.");
					break;
				default:
					throw new NoSuchElementException();
			}

		} catch (MalformedURLException ex) {
			logger.severe(ex.getMessage());
		} catch (IOException ex) {
			logger.severe(ex.getMessage());
		}
		return null;
	}
}
