/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/12/13
 * Time: 11:00 AM
 */
package dataproviders;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OERDataProvider extends DataProvider {

	private final String API_KEY        = "591521376f314ec6925c479cc79ecf4e";
	private final String PROVIDER_URL   = "http://openexchangerates.org/api/";
	private final String LATEST_JSON    = "latest";
	private final String HISTORY_JSON   = "historical";
	private final int REQUEST_TIMEOUT   = 30000;

	@Override
	public String GetLatestRate() {
		return RequestJSON(BuildUrlFrom(LATEST_JSON), REQUEST_TIMEOUT);
	}

	@Override
	public String GetRateAtDate(Date date) {

		SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd");

		String token = String.format("%s/%s", HISTORY_JSON, dt.format(date));
		return (date == null) ? null : RequestJSON(BuildUrlFrom(token), REQUEST_TIMEOUT);
	}

	private String BuildUrlFrom(String token) {

		if (token.isEmpty()) return null;

		StringBuilder sb = new StringBuilder();
		sb.append(PROVIDER_URL);
		sb.append(token);
		sb.append(".json");
		sb.append("?");

		try {
			sb.append(String.format("app_id=%s", URLEncoder.encode(API_KEY, "UTF-8")));
		} catch (UnsupportedEncodingException e) {
			logger.severe("Unable to build URL string.");
			e.printStackTrace();
		}

		return sb.toString();
	}


}
