package dataproviders;

import java.util.NoSuchElementException;

/**
 * Created with IntelliJ IDEA.
 * User: alexa
 * Date: 11/12/13
 * Time: 10:33 AM
 */

public class DataProviderFactory {

	public static DataProvider BuildByType(DataProviderType type) {

		DataProvider ret = null;
		switch (type) {
			case OER:
				ret = new OERDataProvider();
				break;
			case GOOGLE:
				// New data providers will be goes here and needs to be implemented
				break;
			case YAHOO:
				break;
			case ECB:
				break;
			default:
				throw new NoSuchElementException();
		}

		return ret;
	}
}
